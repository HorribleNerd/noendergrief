package com.horriblenerd.noendergrief;

import net.minecraft.entity.monster.EndermanEntity;
import net.minecraft.world.GameRules;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityMobGriefingEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Mod("noendergrief")
public class NoEnderGrief {
    private static final Logger LOGGER = LogManager.getLogger();
    public static GameRules.RuleKey<GameRules.BooleanValue> ENDERMAN_GRIEF;

    public NoEnderGrief() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        try {
            Method booleanCreate = ObfuscationReflectionHelper.findMethod(GameRules.BooleanValue.class, "func_223568_b", boolean.class);
            booleanCreate.setAccessible(true);
            DeferredWorkQueue.runLater(() -> {
                try {
                    Object val = booleanCreate.invoke(GameRules.BooleanValue.class, false);
                    ENDERMAN_GRIEF = GameRules.func_234903_a_("endermanGriefing", GameRules.Category.MOBS, (GameRules.RuleType<GameRules.BooleanValue>) val);
                    LOGGER.info("*Sad Enderman noises*");
                } catch (IllegalAccessException e) {
                    LOGGER.error("IllegalAccessException during FMLClientSetupEvent.");
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    LOGGER.error("InvocationTargetException during FMLClientSetupEvent.");
                    e.printStackTrace();
                }
            });
        } catch (IllegalArgumentException e) {
            LOGGER.error("IllegalArgumentException during FMLClientSetupEvent.");
            e.printStackTrace();
            throw e;
        }
    }

    @SubscribeEvent
    public void onEndermanGrief(EntityMobGriefingEvent event) {
        if (event.getEntity() instanceof EndermanEntity && !event.getEntity().getEntityWorld().getGameRules().getBoolean(ENDERMAN_GRIEF))
            event.setResult(Event.Result.DENY);
    }

}
